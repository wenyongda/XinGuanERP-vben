import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const system: AppRouteModule = {
  path: '/system',
  name: 'System',
  component: LAYOUT,
  redirect: '/system/user',
  meta: {
    orderNo: 1,
    icon: 'ion:settings-outline',
    title: t('routes.sys.moduleName'),
  },
  children: [
    {
      path: 'user',
      name: 'UserManagement',
      meta: {
        title: t('routes.sys.account'),
        ignoreKeepAlive: false,
      },
      component: () => import('/@/views/modules/system/user/index.vue'),
    },
    {
      path: 'user_detail/:id',
      name: 'UserDetail',
      meta: {
        hideMenu: true,
        title: t('routes.sys.account_detail'),
        ignoreKeepAlive: true,
        showMenu: false,
        currentActiveMenu: '/system/user',
      },
      component: () => import('/@/views/modules/system/user/UserDetail.vue'),
    },
    {
      path: 'role',
      name: 'RoleManagement',
      meta: {
        title: t('routes.sys.role'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/modules/system/role/index.vue'),
    },

    {
      path: 'menu',
      name: 'MenuManagement',
      meta: {
        title: t('routes.sys.menu'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/modules/system/menu/index.vue'),
    },
    {
      path: 'dept',
      name: 'DeptManagement',
      meta: {
        title: t('routes.sys.dept'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/modules/system/dept/index.vue'),
    },
    {
      path: 'changePassword',
      name: 'ChangePassword',
      meta: {
        hideMenu: true,
        title: t('routes.sys.password'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/sys/password/index.vue'),
    },
    {
      path: 'dict',
      name: 'DictManagement',
      meta: {
        title: t('routes.sys.dict'),
        ignoreKeepAlive: true,
      },
      component: () => import('/@/views/modules/system/dict/index.vue'),
    },

  ],
};

export default system;
