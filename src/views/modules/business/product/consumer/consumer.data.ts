import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Switch } from 'ant-design-vue';
import { useMessage } from '/@/hooks/web/useMessage';
import { getDictEntryWithListByDictCodeApi } from '/@/api/modules/system/dictEntry';
import pca from '../pca-code.json';
export const columns: BasicColumn[] = [
  {
    title: '物资消费方地址',
    children: [
      {
        title: '省份',
        dataIndex: 'address',
        width: 120,
        customRender: ({ text }) => {
          return text[0];
        },
      },
      {
        title: '市',
        dataIndex: 'address',
        width: 120,
        customRender: ({ text }) => {
          return text[1];
        },
      },
      {
        title: '区县',
        dataIndex: 'address',
        width: 120,
        customRender: ({ text }) => {
          return text[2];
        },
      },
      {
        title: '详细地址',
        dataIndex: 'name',
        width: 180,
      },
    ],
  },
  {
    title: '联系人',
    dataIndex: 'contact',
    width: 100,
  },
  {
    title: '电话',
    dataIndex: 'phone',
    width: 120,
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    width: 180,
  },
  {
    title: '排序',
    dataIndex: 'sort',
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 150,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'address',
    label: '物资消费方省区市',
    component: 'Cascader',
    componentProps: {
      options: pca,
      fieldNames: {
        value: 'name',
        label: 'name',
        children: 'children',
      },
    },
  },
  {
    field: 'phone',
    label: '电话',
    component: 'Input',
    colProps: { span: 8 },
  },
];
// let provincesOptions: any = [];
// Object.keys(pcaa['86']).map(key => {
//   provincesOptions.push({
//     label: pcaa['86'][key],
//     value: key,
//   })
// })
export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    component: 'Input',
    show: false,
  },
  {
    field: 'address',
    label: '选择省区市',
    component: 'Cascader',
    componentProps: {
      options: pca,
      fieldNames: {
        value: 'name',
        label: 'name',
        children: 'children',
      },
    },
  },
  // {
  //   field: 'province',
  //   label: '省份',
  //   required: true,
  //   component: 'Select',
  //   colProps: {
  //     span: 8,
  //   },
  //   componentProps: ({ schema, tableAction, formActionType, formModel }) => {
  //     return {
  //       options: provincesOptions,
  //       placeholder: '省份与城市联动',
  //       onChange: (e: any, v: any) => {
  //         console.log('formActionType', formActionType)
  //         let province = '';
  //         !!formModel.province ? province = formModel.province.toString() : province = v.value.toString();
  //         let citiesOptions: any = []
  //         Object.keys(pcaa[province]).map(key => {
  //           citiesOptions.push({ label: pcaa[province][key], value: key })
  //         })
  //         formModel.city = undefined; //  reset city value
  //         // if (!formModel.province)
  //         // {
  //           const { updateSchema } = formActionType;
  //           updateSchema({
  //             field: 'city',
  //             componentProps: ({ formModel, formActionType }) => {
  //             return {
  //               options: citiesOptions,
  //               onChange: (e: any, v: any) => {
  //                 let areasOptions: any = []
  //                 Object.keys(pcaa[v.value]).map(key => {
  //                   areasOptions.push({ label: pcaa[v.value][key], value: key })
  //                 })
  //                 formModel.area = undefined; //  reset city value
  //                 const { updateSchema } = formActionType;
  //                 updateSchema({
  //                   field: 'area',
  //                   componentProps: {
  //                     options: areasOptions,
  //                   },
  //                 })
  //
  //               },
  //             };
  //             },
  //           })
  //         }
  //
  //
  //       // },
  //     };
  //   },
  //
  // },
  // {
  //   field: 'city',
  //   label: '市',
  //   required: true,
  //   component: 'Select',
  //   colProps: {
  //     span: 8,
  //   },
  //   componentProps: ({ formModel, formActionType }) => {
  //     return {
  //       onChange: (e: any, v: any) => {
  //         console.log(pcaa[v.value])
  //         let areasOptions: any = []
  //         Object.keys(pcaa[v.value]).map(key => {
  //           areasOptions.push({ label: pcaa[v.value][key], value: key })
  //         })
  //         formModel.area = undefined; //  reset city value
  //         const { updateSchema } = formActionType;
  //         updateSchema({
  //           field: 'area',
  //           componentProps: {
  //             options: areasOptions,
  //           },
  //         })
  //
  //       },
  //     };
  //   },
  // },
  // {
  //   field: 'area',
  //   label: '区县',
  //   required: true,
  //   component: 'Select',
  //   colProps: {
  //     span: 8,
  //   },
  // },
  {
    field: 'name',
    label: '详细地址',
    component: 'Input',
    colProps: {
      span: 24,
    },
  },
  {
    field: 'contact',
    label: '联系人',
    component: 'Input',
    colProps: {
      span: 8,
    },
  },
  {
    field: 'phone',
    label: '电话',
    component: 'Input',
    colProps: {
      span: 8,
    },
  },
  {
    field: 'email',
    label: '邮箱',
    component: 'Input',
    colProps: {
      span: 8,
    },
  },
  {
    field: 'sort',
    label: '排序',
    component: 'InputNumber',
    defaultValue: '1',
    colProps: {
      span: 8,
    },
    componentProps: {
      min: 1,
    },
  },
];

// export const provinceComponentProps = (optionsData: any) => {
//   return {
//     componentProps: ({ formModel, formActionType }) => {
//       return {
//         options: optionsData,
//         onchange: (e: any) => {
//           console.log(e)
//           let citiesOptions: any = []
//           Object.keys(pcaa[formModel.province]).map(key => {
//             citiesOptions.push({ label: key, value: pcaa[formModel.province][key] })
//           })
//           formModel.city = undefined; //  reset city value
//           const { updateSchema } = formActionType;
//           updateSchema({
//             field: 'city',
//             componentProps: {
//               options: citiesOptions,
//             },
//           })
//         },
//         getPopupContainer: () => document.body,
//       }
//     }
//   }
// }
