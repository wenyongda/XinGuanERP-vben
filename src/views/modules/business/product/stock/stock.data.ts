import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { getSuppilerWithListApi } from '/@/api/modules/business/product/suppiler';
import { getConsumerWithListApi } from '/@/api/modules/business/product/consumer';
import { getProductWithListApi } from '/@/api/modules/business/product/list';
import { getProductStockWithListApi } from '/@/api/modules/business/product/stock';
export const columns: BasicColumn[] = [
  {
    title: '图片',
    dataIndex: 'imageUrl',
    width: 200,
  },
  {
    title: '物资名称',
    dataIndex: 'name',
    width: 180,
  },
  {
    title: '物资规格',
    dataIndex: 'model',
    width: 120,
    customRender: ({ record }) => {
      return h(Tag, { color: 'green' }, () => record.model);
    },
  },
  {
    title: '物资库存',
    dataIndex: 'stock',
    width: 120,
  },
  {
    title: '单位',
    dataIndex: 'unit',
    width: 120,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'name',
    label: '物资名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'model',
    label: '物资规格',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const inStockFormSchema: FormSchema[] = [
  {
    field: 'suppilerID',
    label: '物资来源',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: getSuppilerWithListApi,
      parms: {
        page: 0,
        pageSize: 0,
      },
      labelField: 'name',
      valueField: 'id',
      immediate: true,
    },
  },
  // {
  //   field: 'type',
  //   label: '入库类型',
  //   component: 'RadioButtonGroup',
  //   defaultValue: '1',
  // },
  {
    field: 'type',
    label: '入库类型',
    slot: 'type',
    component: 'Input',
    required: true,
    defaultValue: '1',
  },
  {
    field: 'productId',
    label: '选择物资',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: getProductWithListApi,
      parms: {
        page: 0,
        pageSize: 0,
      },
      labelField: 'name',
      valueField: 'id',
      immediate: true,
    },
  },

  {
    field: 'productInStock',
    label: '物资数量',
    required: true,
    component: 'InputNumber',
    defaultValue: '1',
    componentProps: {
      min: 1,
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];

export const outStockFormSchema: FormSchema[] = [
  {
    field: 'consumerID',
    label: '物资去向',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: getConsumerWithListApi,
      parms: {
        page: 0,
        pageSize: 0,
      },
      labelField: 'name',
      valueField: 'id',
      immediate: true,
    },
  },
  // {
  //   field: 'type',
  //   label: '出库类型',
  //   component: 'RadioButtonGroup',
  //   defaultValue: '1',
  // },
  {
    field: 'type',
    label: '出库类型',
    slot: 'type',
    required: true,
    component: 'Input',
    defaultValue: '1',
  },
  {
    field: 'productId',
    label: '选择物资',
    required: true,
    component: 'ApiSelect',
    componentProps: {
      api: getProductStockWithListApi,
      parms: {
        page: 0,
        pageSize: 0,
      },
      labelField: 'name',
      valueField: 'productId',
      immediate: true,
    },
  },
  {
    field: 'productOutStock',
    label: '物资数量',
    required: true,
    component: 'InputNumber',
    defaultValue: '1',
    componentProps: {
      min: 1,
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];
