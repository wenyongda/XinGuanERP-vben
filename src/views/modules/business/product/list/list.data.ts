import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';
import { Switch } from 'ant-design-vue';
import { setRoleStatusApi } from '/@/api/modules/system/role';
import { useMessage } from '/@/hooks/web/useMessage';
import { getParentProductCategoryTreeApi } from '/@/api/modules/business/product/category';
import { getDictEntryWithListByDictCodeApi } from '/@/api/modules/system/dictEntry';

export const columns: BasicColumn[] = [
  {
    title: '图片',
    dataIndex: 'imageUrl',
    width: 200,
  },
  {
    title: '物资名称',
    dataIndex: 'name',
    width: 180,
  },
  {
    title: '物资规格',
    dataIndex: 'model',
    width: 120,
    customRender: ({ record }) => {
      return h(Tag, { color: 'green' }, () => record.model);
    },
  },
  {
    title: '单位',
    dataIndex: 'unit',
    width: 120,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 120,
    customRender: ({ record }) => {
      const status = record.status;
      const enable = ~~status === 1;
      const color = enable ? 'green' : 'red';
      const text = enable ? '启用' : '停用';
      return h(Tag, { color: color }, () => text);
    },
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'categoryId',
    label: '物资分类',
    component: 'ApiCascader',
    componentProps: {
      api: getParentProductCategoryTreeApi,
      labelField: 'name',
      valueField: 'id',
      childrenField: 'children',
      isLeaf: (record) => {
        return !(record.lev < 3);
      },
      // getPopupContainer: () => document.body,
    },
  },
  {
    field: 'name',
    label: '物资名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'ApiSelect',
    componentProps: {
      api: getDictEntryWithListByDictCodeApi,
      params: {
        dictCode: 'sys_status',
      },
      labelField: 'label',
      valueField: 'value',
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    component: 'Input',
    show: false,
  },
  {
    field: 'name',
    label: '物资名称',
    required: true,
    component: 'Input',
  },
  {
    field: 'model',
    label: '物资规格',
    required: true,
    component: 'Input',
  },
  {
    field: 'unit',
    label: '单位',
    required: true,
    component: 'Input',
  },
  {
    field: 'categoryId',
    label: '物资分类',
    required: true,
    component: 'Cascader',
    componentProps: {
      changeOnSelect: true,
      fieldNames: { label: 'name', value: 'id', children: 'children' },
      getPopupContainer: () => document.body,
    },
  },
  {
    field: 'status',
    label: '状态',
    slot: 'status',
    required: true,
    component: 'Input',
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
  {
    label: '物资图片',
    field: 'imageUrl',
    slot: 'imageUrl',
    required: true,
    component: 'Input',
  },
];
