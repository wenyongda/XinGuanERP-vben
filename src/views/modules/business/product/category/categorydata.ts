import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: '分类名称',
    dataIndex: 'name',
    width: 160,
    align: 'left',
  },
  {
    title: '排序',
    dataIndex: 'sort',
    width: 50,
  },
  {
    title: '创建时间',
    dataIndex: 'createTime',
    width: 180,
  },
  {
    title: '修改时间',
    dataIndex: 'updateTime',
    width: 180,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '层级',
    width: 80,
    dataIndex: 'lev',
    customRender: ({ record }) => {
      const level = record.lev;
      if (level === 1) {
        return h(Tag, { color: 'green' }, () => '一级');
      } else if (level === 2) {
        return h(Tag, { color: 'blue' }, () => '二级');
      } else if (level === 3) {
        return h(Tag, { color: 'purple' }, () => '三级');
      }
      return level;
    },
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'deptName',
    label: '部门名称',
    component: 'Input',
    colProps: { span: 8 },
  },
  {
    field: 'status',
    label: '状态',
    component: 'Select',
    componentProps: {
      options: [
        { label: '启用', value: '1' },
        { label: '停用', value: '0' },
      ],
    },
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    component: 'Input',
    show: false,
  },
  {
    field: 'name',
    label: '分类名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'pId',
    label: '上级物资',
    component: 'TreeSelect',
    componentProps: {
      fieldNames: {
        value: 'id',
        label: 'name',
        key: 'id',
      },
      getPopupContainer: () => document.body,
    },
    required: false,
  },
  {
    field: 'sort',
    label: '排序',
    component: 'InputNumber',
    required: true,
    defaultValue: '1',
    componentProps: {
      placeholder: '请输入排序',
      min: 1,
    },
  },
  {
    label: '备注',
    field: 'remark',
    component: 'InputTextArea',
  },
];
