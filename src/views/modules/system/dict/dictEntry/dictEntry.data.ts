import { BasicColumn } from '/@/components/Table';
import { FormSchema } from '/@/components/Table';
import { h } from 'vue';
import { Tag } from 'ant-design-vue';

export const columns: BasicColumn[] = [
  {
    title: '字典项名称',
    dataIndex: 'label',
    width: 160,
  },
  {
    title: '字典项值',
    dataIndex: 'value',
    width: 160,
  },
  // {
  //   title: '字典名称',
  //   dataIndex: 'dictName',
  //   width: 160,
  // },
  // {
  //   title: '字典类型',
  //   dataIndex: 'dictType',
  //   width: 160,
  // },
  {
    title: '排序',
    dataIndex: 'orderSort',
    width: 160,
  },
  {
    title: '备注',
    dataIndex: 'desc',
  },
];

export const searchFormSchema: FormSchema[] = [
  {
    field: 'label',
    label: '字典项名称',
    component: 'Input',
    colProps: { span: 8 },
  },
];

export const formSchema: FormSchema[] = [
  {
    field: 'id',
    label: 'Id',
    component: 'Input',
    show: false
  },
  {
    field: 'dictId',
    label: 'DictId',
    component: 'Input',
    show: false
  },
  {
    field: 'label',
    label: '字典项名称',
    component: 'Input',
    required: true,
  },
  {
    field: 'value',
    label: '字典项值',
    component: 'Input',
    required: true,
  },
  {
    field: 'orderSort',
    label: '排序',
    component: 'InputNumber',
    required: true,
    defaultValue: '1',
    componentProps: {
      placeholder: '请输入排序',
      min: 1,
    },
  },
  {
    label: '备注',
    field: 'desc',
    component: 'InputTextArea',
  },
];
