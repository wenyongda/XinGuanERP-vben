import { otherHttp } from '/@/utils/http/axios';

enum Api {
  SearchString = '/string/search'
}

interface StringUnit {
  str1: string,
  str2: string
}

export const searchStringApi = (data: StringUnit) =>
  otherHttp.post({ url: Api.SearchString, data });


