import { DeptParams, DeptListGetResultModel, DeptListItem } from './model/deptModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  GetDeptWithList = '/Dept/GetDeptWithList',
  PostDept = '/Dept/PostDept',
  DeleteDept = '/Dept/DeleteDept',
}

export const getDeptWithListApi = (params: DeptParams) =>
  otherHttp.get<DeptListGetResultModel>({ url: Api.GetDeptWithList, params });

export const postDeptApi = (data: DeptListItem) =>
  otherHttp.post<DeptListItem>({ url: Api.PostDept, data });

export const deleteDeptApi = (params: DeptParams) =>
  otherHttp.delete<DeptParams>({ url: Api.DeleteDept, params }, { joinParamsToUrl: true });
