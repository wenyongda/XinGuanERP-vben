import type { RouteMeta } from 'vue-router';
export interface RouteItem {
  path: string;
  component: any;
  meta: RouteMeta;
  name?: string;
  alias?: string | string[];
  redirect?: string;
  caseSensitive?: boolean;
  children?: RouteItem[];
}

/**
 * @description: Get menu return value
 */
export type getMenuListResultModel = RouteItem[];

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export interface MenuListItem {
  id: string;
  menuType: string;
  menuName: string;
  parentId: string;
  routePath: string;
  component: string;
  permission: string;
  icon: string;
  keepAlive: string;
  linkExternal: string;
  visible: string;
  frame: string;
  linkUrl: string;
  sort: string;
  remarks: string;
  status: string;
  childern?: MenuListItem[];
}

export type MenuParams = BasicPageParams & {
  menuName?: string;
  id?: string;
  nodeId?: string;
}

export type MenuListGetResultModel = BasicFetchResult<MenuListItem>;
