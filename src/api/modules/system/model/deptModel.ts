import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type DeptParams = BasicPageParams & {
  deptName?: string;
  status?: string;
  id?: string;
};

export interface DeptListItem {
  id: string;
  deptName: string;
  orderNo: number;
  status: string;
  parentDept: string;
  createTime: Date;
  remark: string;
  children: DeptListItem[];
}

export type DeptListGetResultModel = BasicFetchResult<DeptListItem>;
