export interface RoleListItem {
  id: string;
  roleName: string;
  roleValue: string;
  status: string;
  orderNo: string;
  createTime: string;
}

import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type RoleListGetResultModel = BasicFetchResult<RoleListItem>;

export type RoleParams = BasicPageParams & {
  id?: string;
  status?: string;
  roleName?: string;
  roleId?: string;
};

export interface RoleMenuListItem {
  menuIds: string[];
  roleId: string;
}
