import { type } from 'os';
import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type UserParams = BasicPageParams & {
  username?: string;
  realName?: string;
  userId?: string;
  deptId?: string;
};

export interface UserListItem {
  userId: string;
  username: string;
  email: string;
  realName: string;
  roleId: string;
  roleName: string;
  createTime: string;
  desc: string;
  status: number;
  deptId: string;
  avatar: string;
  homePath: string;
}

export type UserListGetResultModel = BasicFetchResult<UserListItem>;

export type UserActionLogParams = BasicPageParams & {
  userId?: string;
};

export interface UserActionLogListItem {
  actionId: string;
  actionUserId: string;
  actionUserName: string;
  requestHost: string;
  requestMethod: string;
  requestPath: string;
  actionName: string;
  requestParams: string;
  actionTime: Date;
  responseStatusCode: string;
}

export type UserActionLogListGetResultModel = BasicFetchResult<UserActionLogListItem>;
