import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export interface DictEntryListItem {
  id: string;
  dictId: string;
  label: string;
  value: string;
  desc: string;
  orderSort: number;
  dictName: string;
  dictCode: string;
}

export type DictEntryParams = BasicPageParams & {
  label?: string;
  value?: string;
  id?: string;
  dictCode?: string;
}

export type DictEntryListGetResultModel = BasicFetchResult<DictEntryListItem>;
