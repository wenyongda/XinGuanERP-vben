import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export interface DictListItem {
  id: string;
  name: string;
  code: string;
  desc: string;
  nameType: string;
}

export type DictParams = BasicPageParams & {
  name?: string;
  code?: string;
  id?: string;
}

export type DictListGetResultModel = BasicFetchResult<DictListItem>;
