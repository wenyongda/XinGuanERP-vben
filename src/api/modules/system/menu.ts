import { defHttp, otherHttp } from '/@/utils/http/axios';
import { 
  getMenuListResultModel,
  MenuListItem,
  MenuParams,
  MenuListGetResultModel,
 } from './model/menuModel';

enum Api {
  GetMenuList = '/getMenuList',
  GetSysMenuWithTree = '/SysMenu/GetSysMenuWithTree',
  PostSysMenu = '/SysMenu/PostSysMenu',
  DeleteSysMenu = '/SysMenu/DeleteSysMenu',
  GetSysMenuWithPage = '/SysMenu/GetSysMenuWithPage',
  GetSysMenu = '/sys-menu/sys-menu',
  GetSysMenuAndExcludeNodeWithOptionTree = '/SysMenu/GetSysMenuAndExcludeNodeWithOptionTree',
  GetGrantMenuWithList = '/SysMenu/GetGrantMenuWithList'
}

/**
 * @description: Get user menu based on id
 */

// export const getMenuList = () => {
//   return defHttp.get<getMenuListResultModel>({ url: Api.GetMenuList });
// };

export const getMenuList = () => {
  return otherHttp.get<getMenuListResultModel>({ url: Api.GetGrantMenuWithList });
};

export const getSysMenuWithTreeApi = () => {
  return otherHttp.get<MenuListGetResultModel>({ url: Api.GetSysMenuWithTree });
}

export const postSysMenuApi = (data: MenuListItem) => {
  return otherHttp.post({ url: Api.PostSysMenu, data });
}

export const deleteSysMenuApi = (params: MenuParams) => {
  return otherHttp.delete({ url: Api.DeleteSysMenu, params }, { joinParamsToUrl: true });
}

export const getSysMenuWithPageApi = (params: MenuParams) => {
  return otherHttp.get<MenuListGetResultModel>({ url: Api.GetSysMenuWithPage, params });
}

export const getSysMenuAndExcludeNodeWithOptionTreeApi = (params: MenuParams) => {
  return otherHttp.get<MenuListGetResultModel>({ url: Api.GetSysMenuAndExcludeNodeWithOptionTree, params });
}
