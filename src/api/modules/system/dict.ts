import { 
  DictListGetResultModel,
  DictListItem,
  DictParams
} from './model/dictModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  GetDictWithList = '/Dict/GetDictWithList',
  DeleteDict = '/Dict/DeleteDict',
  PostDict = '/Dict/PostDict',
  GetDictWithPage = '/Dict/GetDictWithPage'
}

export const getDictWithListApi = () =>
  otherHttp.get<DictListGetResultModel>({ url: Api.GetDictWithList });

export const postDictApi = (data: DictListItem) =>
  otherHttp.post({ url: Api.PostDict, data });

export const deleteDictApi = (params: DictParams) =>
  otherHttp.delete({ url: Api.DeleteDict, params }, { joinParamsToUrl: true });

export const getDictWithPageApi = (params: DictParams) =>
  otherHttp.get<DictListGetResultModel>({ url: Api.GetDictWithPage, params });
