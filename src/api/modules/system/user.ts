import { 
  UserParams,
  UserListGetResultModel,
  UserListItem,
  UserActionLogParams,
  UserActionLogListGetResultModel
} from './model/userModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  GetUserWithPage = '/User/GetUserWithPage',
  GetUserWithList = '/User/GetUserWithList',
  PostUser = '/User/PostUser',
  DeleteUser = '/User/DeleteUser',
  GetSysActionLog = '/SysActionLog/GetSysActionLog',
  GetUserDetail = '/User/GetUserDetail',
  ResetToDefaultPassword = '/User/ResetToDefaultPassword',
}

export const getUserWithPageApi = (params: UserParams) =>
  otherHttp.get<UserListGetResultModel>({ url: Api.GetUserWithPage, params });

export const getUserWithListApi = (params: UserParams) =>
  otherHttp.get<UserListGetResultModel>({ url: Api.GetUserWithList, params });

export const postUserApi = (data: UserListItem) =>
  otherHttp.post<UserListItem>({ url: Api.PostUser, data });

export const deleteUserApi = (params: UserParams) =>
  otherHttp.delete<UserParams>({ url: Api.DeleteUser, params }, { joinParamsToUrl: true });

export const getSysActionLogApi = (params: UserActionLogParams) =>
  otherHttp.get<UserActionLogListGetResultModel>({ url: Api.GetSysActionLog, params });

export const getUserDetailApi = (params: UserParams) =>
  otherHttp.get<UserListItem>({ url: Api.GetUserDetail, params });

export const resetToDefaultPasswordApi = (params: UserParams) =>
  otherHttp.put({ url: Api.ResetToDefaultPassword, params}, { joinParamsToUrl: true });
