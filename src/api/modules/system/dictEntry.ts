import { 
  DictEntryListGetResultModel,
  DictEntryListItem,
  DictEntryParams
} from './model/dictEntryModel';

import { otherHttp } from '/@/utils/http/axios';

enum Api {
  // GetAllDictEntryList = '/DictEntry/dict-entry',
  DeleteDictEntry = '/DictEntry/DeleteDictEntry',
  PostDictEntry = '/DictEntry/PostDictEntry',
  GetDictEntryWithPage = '/DictEntry/GetDictEntryWithPage',
  GetDictEntryWithListByDictCode = '/DictEntry/GetDictEntryWithListByDictCode'
}

// export const getAllDictEntryListApi = () =>
//   otherHttp.get<DictEntryListGetResultModel>({ url: Api.GetAllDictEntryList });

export const postDictEntryApi = (data: DictEntryListItem) =>
  otherHttp.post({ url: Api.PostDictEntry, data });

export const deleteDictEntryApi = (params: DictEntryParams) =>
  otherHttp.delete({ url: Api.DeleteDictEntry, params }, { joinParamsToUrl: true });

export const getDictEntryWithPageApi = (params: DictEntryParams) =>
  otherHttp.get<DictEntryListGetResultModel>({ url: Api.GetDictEntryWithPage, params });

export const getDictEntryWithListByDictCodeApi = (params: DictEntryParams) =>
  otherHttp.get<DictEntryListGetResultModel>({ url: Api.GetDictEntryWithListByDictCode, params });
