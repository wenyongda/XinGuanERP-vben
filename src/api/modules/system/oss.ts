import { otherHttp } from '/@/utils/http/axios';

enum Api {
  GetOssToken = '/Oss/GetOssToken'
}

export const getOssTokenApi = () =>
  otherHttp.get({ url: Api.GetOssToken });
