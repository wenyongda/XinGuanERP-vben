import { otherHttp } from '/@/utils/http/axios';
import { RoleListGetResultModel, RoleListItem, RoleParams, RoleMenuListItem } from './model/roleModel';
enum Api {
  GetRoleWithPage = '/Role/GetRoleWithPage',
  GetRoleWithList = '/Role/GetRoleWithList',
  PostRole = '/Role/PostRole',
  DeleteRole = '/Role/DeleteRole',
  SetRoleStatus = '/Role/SetRoleStatus',
  GetSysMenuByRoleIdWithOptionTree = '/Role/GetSysMenuByRoleIdWithOptionTree',
  PostAssignMenu = '/Role/PostAssignMenu',
}
export const getRoleWithListApi = (params: RoleParams) =>
  otherHttp.get<RoleListGetResultModel>({ url: Api.GetRoleWithList, params });

export const getRoleWithPageApi = (params: RoleParams) =>
  otherHttp.get<RoleListGetResultModel>({ url: Api.GetRoleWithPage, params });

export const postRoleApi = (data: RoleListItem) =>
  otherHttp.post<RoleListItem>({ url: Api.PostRole, data });

export const deleteRoleApi = (params: RoleParams) =>
  otherHttp.delete<RoleParams>({ url: Api.DeleteRole, params }, { joinParamsToUrl: true });

export const setRoleStatusApi = (data: RoleParams) =>
  otherHttp.put<RoleParams>({ url: Api.SetRoleStatus, data });

export const getSysMenuByRoleIdWithOptionTreeApi = (params: RoleParams) =>
  otherHttp.get({ url: Api.GetSysMenuByRoleIdWithOptionTree, params });

export const postAssignMenuApi = (data: RoleMenuListItem) =>
  otherHttp.post({ url: Api.PostAssignMenu, data });
