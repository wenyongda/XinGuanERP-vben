import { StockParams, StockListGetResultModel, StockListItem } from './model/stockModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  InProductStock = '/ProductStock/InProductStock',
  OutProductStock = '/ProductStock/OutProductStock',
  GetProductStockWithPage = '/ProductStock/GetProductStockWithPage',
  GetProductStockWithList = '/ProductStock/GetProductStockWithList',
}

export const getProductStockWithPageApi = (params: StockParams) =>
  otherHttp.get<StockListGetResultModel>({ url: Api.GetProductStockWithPage, params });

export const inProductStockApi = (data: StockListItem) =>
  otherHttp.post({ url: Api.InProductStock, data });

export const outProductStockApi = (params: StockListItem) =>
  otherHttp.post({ url: Api.OutProductStock, params });

export const getProductStockWithListApi = () =>
  otherHttp.get<StockListGetResultModel>({ url: Api.GetProductStockWithList })
