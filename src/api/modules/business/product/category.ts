import { ProductCategoryParams, ProductCategoryListGetResultModel, ProductCategoryListItem } from './model/categoryModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  UpdateProductCategory = '/ProductCategory/UpdateProductCategory',
  AddProductCategory = '/ProductCategory/AddProductCategory',
  DeleteProductCategory = '/ProductCategory/DeleteProductCategory',
  GetProductCategoryTreeWithPage = '/ProductCategory/GetProductCategoryTreeWithPage',
  GetParentProductCategoryTree = '/ProductCategory/GetParentProductCategoryTree',
}

export const getProductCategoryTreeWithPageApi = (params: ProductCategoryParams) =>
  otherHttp.get<ProductCategoryListGetResultModel>({ url: Api.GetProductCategoryTreeWithPage, params });

export const addProductCategoryApi = (data: ProductCategoryListItem) =>
  otherHttp.post({ url: Api.AddProductCategory, data });

export const deleteProductCategoryApi = (params: ProductCategoryParams) =>
  otherHttp.delete({ url: Api.DeleteProductCategory, params }, { joinParamsToUrl: true });

export const updateProductCategoryApi = (data: ProductCategoryListItem) =>
  otherHttp.put({ url: Api.UpdateProductCategory, data });

export const getParentProductCategoryTreeApi = () => 
  otherHttp.get<ProductCategoryListItem>({ url: Api.GetParentProductCategoryTree });
