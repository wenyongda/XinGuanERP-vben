import { ProductParams, ProductListGetResultModel, ProductListItem } from './model/listModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  UpdateProduct = '/Product/UpdateProduct',
  AddProduct = '/Product/AddProduct',
  DeleteProduct = '/Product/DeleteProduct',
  GetProductWithPage = '/Product/GetProductWithPage',
  GetProductWithList = '/Product/GetProductWithList',
}

export const getProductWithPageApi = (params: ProductParams) =>
  otherHttp.post<ProductListGetResultModel>({ url: Api.GetProductWithPage, params });

export const addProductApi = (data: ProductListItem) =>
  otherHttp.post<ProductListItem>({ url: Api.AddProduct, data });

export const deleteProductApi = (params: ProductParams) =>
  otherHttp.delete<ProductParams>({ url: Api.DeleteProduct, params }, { joinParamsToUrl: true });

export const updateProductApi = (data: ProductListItem) =>
  otherHttp.put<ProductListItem>({ url: Api.UpdateProduct, data });

export const getProductWithListApi = () =>
  otherHttp.get<ProductListGetResultModel>({ url: Api.GetProductWithList });
