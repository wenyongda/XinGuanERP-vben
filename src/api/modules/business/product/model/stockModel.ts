import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type StockParams = BasicPageParams & {
  name?: string;
  id?: string;
};

export interface StockListItem {
  id: string;
  productId: string;
  stock: string;
  name: string;
  model: string;
  unit: string;
  type: string;
  suppilerId: string;
  consumerId: string;
  productInStock: number;
  productOutStock: number;
  remark: string;
  imageUrl: string;
  oneCategoryId: string;
  twoCategoryId: string;
  threeCategoryId: string;
}

export type StockListGetResultModel = BasicFetchResult<StockListItem>;
