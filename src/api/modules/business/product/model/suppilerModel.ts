import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type SuppilerParams = BasicPageParams & {
  name?: string;
  id?: string;
  address?: string[];
  phone?: string;
};

export interface SuppilerListItem {
  id: string;
  address: string[];
  name: string;
  phone: string;
  contact: string;
  email: string;
  createTime: Date;
}

export type SuppilerListGetResultModel = BasicFetchResult<SuppilerListItem>;
