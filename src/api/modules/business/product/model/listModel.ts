import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type ProductParams = BasicPageParams & {
  name?: string;
  id?: string;
  categoryId?: string;
};

export interface ProductListItem {
  id: string;
  name: string;
  model: string;
  unit: string;
  remark: string;
  sort: number;
  oneCategoryId: string;
  twoCategoryId: string;
  threeCategoryId: string;
  categoryId: string[];
  imageUrl: string;
  status: string;
  createTime: Date;
  categoryKeys: string[];
}

export type ProductListGetResultModel = BasicFetchResult<ProductListItem>;
