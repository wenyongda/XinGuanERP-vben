import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type ProductCategoryParams = BasicPageParams & {
  name?: string;
  id?: string;
};

export interface ProductCategoryListItem {
  id: string;
  name: string;
  sort: number;
  pId: string;
  createTime: Date;
  remark: string;
  lev: number;
  children: ProductCategoryListItem[];
}

export type ProductCategoryListGetResultModel = BasicFetchResult<ProductCategoryListItem>;
