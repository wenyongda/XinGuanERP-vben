import { BasicPageParams, BasicFetchResult } from '/@/api/model/baseModel';

export type ConsumerParams = BasicPageParams & {
  name?: string;
  id?: string;
  address?: string[];
  phone?: string;
};

export interface ConsumerListItem {
  id: string;
  address: string[];
  name: string;
  phone: string;
  contact: string;
  email: string;
  createTime: Date;
}

export type ConsumerListGetResultModel = BasicFetchResult<ConsumerListItem>;
