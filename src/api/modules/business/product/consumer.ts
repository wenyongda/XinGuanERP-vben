import {
  ConsumerParams,
  ConsumerListGetResultModel,
  ConsumerListItem,
} from './model/consumerModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  UpdateConsumer = '/Consumer/UpdateConsumer',
  AddConsumer = '/Consumer/AddConsumer',
  DeleteConsumer = '/Consumer/DeleteConsumer',
  GetConsumerWithPage = '/Consumer/GetConsumerWithPage',
  GetConsumerWithList = '/Consumer/GetConsumerWithList',
}

export const getConsumerWithPageApi = (params: ConsumerParams) =>
  otherHttp.post<ConsumerListGetResultModel>({ url: Api.GetConsumerWithPage, data: params });

export const addConsumerApi = (data: ConsumerListItem) =>
  otherHttp.post({ url: Api.AddConsumer, data });

export const deleteConsumerApi = (params: ConsumerParams) =>
  otherHttp.delete({ url: Api.DeleteConsumer, params }, { joinParamsToUrl: true });

export const updateConsumerApi = (data: ConsumerListItem) =>
  otherHttp.put({ url: Api.UpdateConsumer, data });

export const getConsumerWithListApi = () =>
  otherHttp.get<ConsumerListGetResultModel>({ url: Api.GetConsumerWithList });
