import {
  SuppilerParams,
  SuppilerListGetResultModel,
  SuppilerListItem,
} from './model/suppilerModel';
import { otherHttp } from '/@/utils/http/axios';

enum Api {
  UpdateSuppiler = '/Suppiler/UpdateSuppiler',
  AddSuppiler = '/Suppiler/AddSuppiler',
  DeleteSuppiler = '/Suppiler/DeleteSuppiler',
  GetSuppilerWithPage = '/Suppiler/GetSuppilerWithPage',
  GetSuppilerWithList = '/Suppiler/GetSuppilerWithList',
}

export const getSuppilerWithPageApi = (params: SuppilerParams) =>
  otherHttp.post<SuppilerListGetResultModel>({ url: Api.GetSuppilerWithPage, data: params });

export const addSuppilerApi = (data: SuppilerListItem) =>
  otherHttp.post({ url: Api.AddSuppiler, data });

export const deleteSuppilerApi = (params: SuppilerParams) =>
  otherHttp.delete({ url: Api.DeleteSuppiler, params }, { joinParamsToUrl: true });

export const updateSuppilerApi = (data: SuppilerListItem) =>
  otherHttp.put({ url: Api.UpdateSuppiler, data });

export const getSuppilerWithListApi = () =>
  otherHttp.get<SuppilerListGetResultModel>({ url: Api.GetSuppilerWithList });
