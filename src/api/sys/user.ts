import { defHttp, otherHttp } from '/@/utils/http/axios';
import { LoginParams, LoginResultModel, GetUserInfoModel } from './model/userModel';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  // Login = '/login',
  Login = '/UserLogin/Login',
  // Logout = '/logout',
  Logout = '/UserLogin/Logout',
  // GetUserInfo = '/getUserInfo',
  GetUserInfo = '/UserLogin/GetUserInfo',
  GetPermCode = '/getPermCode',
  TestRetry = '/testRetry',
  GetPermCodeWithList = '/UserLogin/GetPermCodeWithList',
}

/**
 * @description: user login api
 */
// export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
//   return defHttp.post<LoginResultModel>(
//     {
//       url: Api.Login,
//       params,
//     },
//     {
//       errorMessageMode: mode,
//     },
//   );
// }

export function loginApi(params: LoginParams, mode: ErrorMessageMode = 'modal') {
  return otherHttp.post<LoginResultModel>(
    {
      url: Api.Login,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: getUserInfo
 */
// export function getUserInfo() {
//   return defHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
// }
export function getUserInfo() {
  return otherHttp.get<GetUserInfoModel>({ url: Api.GetUserInfo }, { errorMessageMode: 'none' });
}

// export function getPermCode() {
//   return defHttp.get<string[]>({ url: Api.GetPermCode });
// }

export function getPermCode() {
  return otherHttp.get<string[]>({ url: Api.GetPermCodeWithList });
}

// export function doLogout() {
//   return defHttp.get({ url: Api.Logout });
// }
export function doLogout() {
  return otherHttp.get({ url: Api.Logout });
}

export function testRetry() {
  return defHttp.get(
    { url: Api.TestRetry },
    {
      retryRequest: {
        isOpenRetry: true,
        count: 5,
        waitTime: 1000,
      },
    },
  );
}
