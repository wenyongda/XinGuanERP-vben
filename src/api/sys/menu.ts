import { defHttp, otherHttp } from '/@/utils/http/axios';
import { getMenuListResultModel } from './model/menuModel';

enum Api {
  GetMenuList = '/getMenuList',
  GetGrantMenuWithList = '/SysMenu/GetGrantMenuWithList',
}

/**
 * @description: Get user menu based on id
 */

// export const getMenuList = () => {
//   return defHttp.get<getMenuListResultModel>({ url: Api.GetMenuList });
// };

export const getMenuList = () => {
  return otherHttp.get<getMenuListResultModel>({ url: Api.GetGrantMenuWithList });
};
