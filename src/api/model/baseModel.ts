export interface BasicPageParams {
  page?: number;
  pageSize?: number;
}

export interface BasicFetchResult<T> {
  [x: string]: any;
  items: T[];
  total: number;
}
