FROM nginx:1.23.2
COPY dist/ /usr/share/nginx/html/dist/
COPY nginx.conf /etc/nginx/nginx.conf
COPY conf.d/ /etc/nginx/conf.d/
COPY cert/ /usr/local/nginx/cert/
